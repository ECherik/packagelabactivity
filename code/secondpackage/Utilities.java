/**
 * Doubles he input value
 * 
 * @param x The value to be doubled
 * @return The value of x times two
 */
package secondpackage;

public class Utilities {
    

    public int doubleMe(int x){
        int value = x*2;
        return value;
    }
}
