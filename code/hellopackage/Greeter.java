/**
 * Greeter is a class used for testing packages out
 * information about a rectangle
 * @author Shuya Liu
 * @version 5/13/2020
*/
package hellopackage;
import java.util.Scanner;
import java.util.Random;
import secondpackage.Utilities;
public class Greeter{
    public static void main(String[]args){
        Scanner reader = new Scanner(System.in);
        Random random = new Random();
        Utilities util = new Utilities();
        int number;

        System.out.println("Enter an number:");
        number = reader.nextInt();

        System.out.println(util.doubleMe(number));
    }

}